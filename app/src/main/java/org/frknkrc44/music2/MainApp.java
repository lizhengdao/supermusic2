package org.frknkrc44.music2;

import android.app.*;
import android.content.*;
import android.content.pm.*;
import android.media.*;
import org.blinksd.sdb.*;

public class MainApp extends Application {
	
	private static MainApp app;
	private static SuperMiniDB smdb;
	private static MediaPlayer mPlayer;
	private static BroadcastReceiver mediaAct;
	private static int audioSessionId;

	@Override
	public void onCreate(){
		app = this;
		smdb = new SuperMiniDB(getPackageName(),getFilesDir());
		mPlayer = new MediaPlayer();
		super.onCreate();
	}
	
	public static final MainApp getContext(){
		return app;
	}
	
	public static final PackageManager getPackageMgr(){
		return app.getPackageManager();
	}
	
	public static final ContentResolver getResolver(){
		return app.getContentResolver();
	}
	
	public static final SuperMiniDB getDatabase(){
		return smdb;
	}
	
	public static final MediaPlayer getMediaPlayer(){
		return mPlayer;
	}
	
	public static BroadcastReceiver getMediaActionReceiver(){
		return mediaAct;
	}
	
	public static void setMediaActionReceiver(BroadcastReceiver recv){
		mediaAct = recv;
	}
	
	public static int getAudioSessionId(){
		return mPlayer.getAudioSessionId();
	}

}
