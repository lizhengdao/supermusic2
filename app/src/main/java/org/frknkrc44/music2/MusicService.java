package org.frknkrc44.music2;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.media.*;
import android.media.session.*;
import android.net.*;
import android.os.*;
import android.provider.*;
import android.view.*;
import android.widget.*;
import java.lang.reflect.*;
import java.util.*;
import java.util.stream.*;
import org.frknkrc44.music2.adapter.*;
import org.frknkrc44.music2.holder.*;
import org.frknkrc44.music2.util.*;
import org.frknkrc44.music2.util.MusicListUtils.*;
import java.lang.ref.*;
import android.media.audiofx.*;

public class MusicService extends Service implements AudioManager.OnAudioFocusChangeListener {
	
	private MusicServiceBinder binder;
	private MediaPlayer mPlayer;
	private MusicList mItems;
	private long mCurrentTrack = 0, mOldTrack = -1;
	private int mLastPos = 0;
	private Notification.Builder notify;
	
	public static final String PLAY_ACTION = "org.frknkrc44.music2.action.PLAY";
	public static final String PREV_ACTION = "org.frknkrc44.music2.action.PREV";
	public static final String NEXT_ACTION = "org.frknkrc44.music2.action.NEXT";
	public static final String STOP_ACTION = "org.frknkrc44.music2.action.STOP";
	public static final String REPT_ACTION = "org.frknkrc44.music2.action.REPT";
	public static final String SHUF_ACTION = "org.frknkrc44.music2.action.SHUF";
	
	public static final String QUEUE_CHANGED = "org.frknkrc44.music2.action.QUEUE_CHANGED";
	
	private static Intent queueChanged;
	
	private String channelID = getClass().getName();
	
	private MediaSession session;
	
	private int mShuffleMode = 0, mLoopMode = 0;
	
	private PendingIntent prevPi,playPi,nextPi,openAppPi,reptPi,shufPi;
	private Notification.MediaStyle mediaStyle;
	
	private IntentFilter playerIntentFilter;
	
	private boolean volumeReduced, playerPlayNext, init;
	
	private static final float reducedVolume = 0.75f;
	
	private Bitmap lastBitmap, defaultBitmap;
	private long lastAlbumId = -100;
	
	@Override
	public void onCreate(){
		super.onCreate();
		if(mPlayer == null)
			mPlayer = MainApp.getMediaPlayer();
		prepareList();
		mCurrentTrack = getLong("currentTrack", 0);
		mLoopMode = getInt("loopMode", 0);
		mShuffleMode = getInt("shuffleMode", 0);
		if(session == null){
			session = new MediaSession(this,getPackageName());
			session.setActive(true);
			session.setCallback(new MediaSession.Callback(){
					@Override
					public boolean onMediaButtonEvent(Intent intent){
						String intentAction = intent.getAction();
						if (Intent.ACTION_MEDIA_BUTTON.equals(intentAction)) {
							KeyEvent event = intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
							if (event != null) {
								boolean handled = true;
								switch(event.getKeyCode()){
									case KeyEvent.KEYCODE_MEDIA_PLAY:
									case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
										play();
										break;
									case KeyEvent.KEYCODE_MEDIA_PAUSE:
									case KeyEvent.KEYCODE_HEADSETHOOK:
										pause();
										break;
									case KeyEvent.KEYCODE_MEDIA_STOP:
										stop();
										break;
									case KeyEvent.KEYCODE_MEDIA_NEXT:
										next();
										break;
									case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
										prev();
										break;
									case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
										ffwd();
										break;
									case KeyEvent.KEYCODE_MEDIA_REWIND:
										rewd();
										break;
									default:
										handled = false;
										break;
								}
								return handled;
							}
						}
						return false;
					}
				});
		}
		mPlayer.setWakeMode(this, PowerManager.PARTIAL_WAKE_LOCK);
		mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
			@Override
			public void onCompletion(MediaPlayer p1){
				if(init){
					playerPlayNext = true;
					next();
				}
			}
		});
		
		mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener(){
			@Override
			public void onPrepared(MediaPlayer p1){
				if(init && playerPlayNext){
					playerPlayNext = false;
					p1.start();
					startForeground();
					sendPlayerBroadcast();
				}
			}
		});
		
		mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener(){
			@Override
			public boolean onError(MediaPlayer p1, int p2, int p3){
				if(init){
					Toast.makeText(MusicService.this,String.format(getString(R.string.player_error),getTitle()),Toast.LENGTH_SHORT).show();
					next();
				}
				return true;
			}
		});
	}
	
	@Override
	public void onAudioFocusChange(int state){
		switch(state){
			case AudioManager.AUDIOFOCUS_NONE:
			case AudioManager.AUDIOFOCUS_LOSS:
			case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
				pause();
				break;
			case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
				volumeReduced = true;
				mPlayer.setVolume(reducedVolume,reducedVolume);
				break;
			case AudioManager.AUDIOFOCUS_GAIN:
				if(volumeReduced){
					mPlayer.setVolume(1,1);
				}
				break;
		}
	}

	@Override
	public IBinder onBind(Intent p1){
		if(binder == null){
			binder = new MusicServiceBinder(this);
		}
		return binder;
	}

	@Override
	public void onTrimMemory(int level){
		super.onTrimMemory(level);
		if(level >= TRIM_MEMORY_BACKGROUND){
			onDestroy();
			System.exit(0);
		}
	}
	
	private void prepareList(){
		MusicListReadyListener listener = new MusicListReadyListener(){
			@Override
			public void onReady(MusicList items){
				mItems = items;
			}
		};
		MusicListUtils.prepareMusicList(listener);
	}
	
	private void setNotification(){
		if(prevPi == null){
			prevPi = PendingIntent.getBroadcast(this, 0, new Intent(PREV_ACTION), 0);
			playPi = PendingIntent.getBroadcast(this, 0, new Intent(PLAY_ACTION), 0);
			nextPi = PendingIntent.getBroadcast(this, 0, new Intent(NEXT_ACTION), 0);
			openAppPi = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
			reptPi = PendingIntent.getBroadcast(this, 0, new Intent(REPT_ACTION), 0);
			shufPi = PendingIntent.getBroadcast(this, 0, new Intent(SHUF_ACTION), 0);
		}
		if(notify == null){
			notify = new Notification.Builder(this);
			notify.setAutoCancel(false);
			notify.setVisibility(Notification.VISIBILITY_PUBLIC);
			notify.setVibrate(new long[]{0});
			if(mediaStyle == null){
				mediaStyle = new Notification.MediaStyle();
				mediaStyle.setShowActionsInCompactView(1,2,3);
				mediaStyle.setMediaSession(session.getSessionToken());
				notify.setStyle(mediaStyle);
			}
			notify.setSmallIcon(android.R.drawable.ic_media_play);
			notify.setContentIntent(openAppPi);
			if(Build.VERSION.SDK_INT >= 26){
				NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
				NotificationChannel channel = manager.getNotificationChannel(channelID);
				if(channel == null){
					channel = new NotificationChannel(channelID,getClass().getSimpleName(),NotificationManager.IMPORTANCE_LOW);
					manager.createNotificationChannel(channel);
				}
				notify.setChannelId(channelID);
			}
		}
		if(mPlayer.isPlaying() && Build.VERSION.SDK_INT < 29){
			String subText = SongListAdapter.calculateTime(getDuration());
			if(mPlayer.isLooping()){
				subText += " / ∞";
				notify.setShowWhen(false);
				notify.setUsesChronometer(false);
			} else {
				notify.setShowWhen(true);
				notify.setUsesChronometer(true);
				notify.setWhen(System.currentTimeMillis() - getPosition());
			}
			notify.setSubText(subText);
		} else {
			notify.setSubText(null);
			notify.setShowWhen(false);
			notify.setUsesChronometer(false);
		}
		AudioManager audMgr = (AudioManager) getSystemService(AUDIO_SERVICE);
		if(isPlaying()){
			audMgr.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
		} else {
			audMgr.abandonAudioFocus(this);
		}
		controlAudioEffects();
		MusicItem item = getMusicItem();
		if(item == null){
			return;
		}
		Bitmap art = getAlbumArt();
		session.setMetadata(getMetadata(art));
		session.setPlaybackState(getState());
		clearNotificationActionList();
		notify.addAction(getLoopImageResource(), "Loop", reptPi);
		notify.addAction(ThemeAdapter.getPreviousImageResource(), "Prev", prevPi);
		notify.addAction(ThemeAdapter.getPlayImageResource(isPlaying()), "Play/Pause", playPi);
		notify.addAction(ThemeAdapter.getNextImageResource(), "Next", nextPi);
		notify.addAction(getShuffleImageResource(), "Shuffle", shufPi);
		notify.setContentTitle(item.title);
		notify.setContentText(item.artist + " - " + item.album);
		notify.setLargeIcon(art);
	}
	
	private void clearNotificationActionList(){
		try {
			Field actionsField = Notification.Builder.class.getDeclaredField("mActions");
			actionsField.setAccessible(true);
			List mActions = (List) actionsField.get(notify);
			mActions.clear();
		} catch(Throwable t){}
	}
	
	private PlaybackState getState(){
		PlaybackState.Builder builder = new PlaybackState.Builder();
		builder.setActions(PlaybackState.ACTION_PLAY |
							PlaybackState.ACTION_PAUSE |
							PlaybackState.ACTION_PLAY_PAUSE |
							PlaybackState.ACTION_SKIP_TO_PREVIOUS |
							PlaybackState.ACTION_SKIP_TO_NEXT |
							PlaybackState.ACTION_FAST_FORWARD |
							PlaybackState.ACTION_REWIND |
							PlaybackState.ACTION_STOP);
		int state = isPlaying() 
						? PlaybackState.STATE_PLAYING
						: PlaybackState.STATE_PAUSED;
		builder.setState(state,getPosition(),1);
		return builder.build();
	}
	
	private MediaMetadata getMetadata(Bitmap art){
		MediaMetadata.Builder builder = new MediaMetadata.Builder();
		builder.putLong(MediaMetadata.METADATA_KEY_DURATION, getDuration());
		builder.putString(MediaMetadata.METADATA_KEY_TITLE, getTitle());
		builder.putString(MediaMetadata.METADATA_KEY_ARTIST, getArtist());
		builder.putString(MediaMetadata.METADATA_KEY_ALBUM, getAlbum());
		if(getBoolean(SettingMap.SET_KEY_SHOW_ALBUM_ART_ON_LOCK))
			builder.putBitmap(MediaMetadata.METADATA_KEY_ALBUM_ART, art);
		return builder.build();
	}
	
	private int getLoopImageResource(){
		return ThemeAdapter.getLoopImageResource(getRepeatMode());
	}
	
	private int getShuffleImageResource(){
		return ThemeAdapter.getShuffleImageResource(getShuffleMode());
	}
	
	private void startForeground(){
		setNotification();
		if(isPlaying()){
			startForeground(channelID.hashCode(),notify.getNotification());
		} else {
			stopForeground(false);
			NotificationManager nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			nManager.notify(channelID.hashCode(),notify.getNotification());
		}
		
		if(playerIntentFilter == null){
			playerIntentFilter = new IntentFilter(PREV_ACTION);
			playerIntentFilter.addAction(PLAY_ACTION);
			playerIntentFilter.addAction(NEXT_ACTION);
			playerIntentFilter.addAction(STOP_ACTION);
			playerIntentFilter.addAction(REPT_ACTION);
			playerIntentFilter.addAction(SHUF_ACTION);
			playerIntentFilter.addAction(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
		}
		
		if(mediaActionReceiver == null){
			if(MainApp.getMediaActionReceiver() == null){
				MainApp.setMediaActionReceiver(new BroadcastReceiver(){
					@Override
					public void onReceive(Context p1, Intent p2){
						playerPlayNext = true;
						switch(p2.getAction()){
							case PREV_ACTION:
								prev();
								break;
							case PLAY_ACTION:
								play();
								break;
							case NEXT_ACTION:
								next();
								break;
							case STOP_ACTION:
								stop();
								break;
							case REPT_ACTION:
								setRepeatMode((getRepeatMode() + 1) % 3);
								break;
							case SHUF_ACTION:
								setShuffleMode((getShuffleMode() + 1) % 2);
								break;
							case AudioManager.ACTION_AUDIO_BECOMING_NOISY:
								pause();
								break;
						}
					}
				});
			}
			mediaActionReceiver = MainApp.getMediaActionReceiver();
		}
		registerReceiver(mediaActionReceiver,playerIntentFilter);
	}

	@Override
	public boolean onUnbind(Intent intent){
		return super.onUnbind(intent);
	}

	@Override
	public void onTaskRemoved(Intent rootIntent){
		super.onTaskRemoved(rootIntent);
		onDestroy();
	}

	@Override
	public void onDestroy(){
		stop();
		stopForeground();
		super.onDestroy();
		stopSelf();
	}
	
	private void stopForeground(){
		stopForeground(true);
		try {
			unregisterReceiver(mediaActionReceiver);
			NotificationManager nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			nManager.cancel(channelID.hashCode());
		} catch(Throwable t){}
	}
	
	public void play(){
		if(mCurrentTrack == mOldTrack){
			if(isPlaying()){
				pause();
			} else {
				resume();
			}
			sendPlayerBroadcast();
			return;
		} else {
			if(getRepeatMode() == 2){
				setRepeatMode(1);
			}
		}
		mOldTrack = mCurrentTrack;
		MusicItem item = getMusicItem();
		try {
			mPlayer.reset();
			mPlayer.setDataSource(this, Uri.parse(item.data));
			mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			playerPlayNext = init = true;
			mPlayer.prepareAsync();
			//MainApp.setAudioSessionId(mPlayer.getAudioSessionId());
			putLong("currentTrack",item.songId);
		} catch(Throwable t){}
	}
	
	private void sendPlayerBroadcast(){
		if(queueChanged == null){
			queueChanged = new Intent(QUEUE_CHANGED);
		}
		queueChanged.putExtra("currentTrack",mCurrentTrack);
		sendBroadcast(queueChanged);
	}
	
	public void playAt(int index){
		if(mItems == null){
			prepareList();
			return;
		}
		if(index >= 0 || index < mItems.size()){
			mCurrentTrack = mItems.getSongId(index);
			play();
		}
	}
	
	public void playFromId(long songId){
		if(mItems == null){
			prepareList();
			return;
		}
		for(MusicItem item : mItems){
			if(item.songId == songId){
				playAt(mItems.indexOf(item));
				break;
			}
		}
	}
	
	public void pause(){
		if(isPlaying()){
			mPlayer.pause();
		}
		startForeground();
	}
	
	public void resume(){
		if(!isPlaying()){
			mPlayer.start();
		}
		startForeground();
	}
	
	public void stop(){
		if(isPlaying()){
			pause();
		}
		moveStart();
		sendPlayerBroadcast();
		stopForeground();
	}
	
	public void moveStart(){
		setPosition(0);
	}
	
	public void prev(){
		if(mItems == null){
			prepareList();
			return;
		}
		if(getPosition() > 2000){
			moveStart();
			return;
		}
		if(getShuffleMode() == 0){
			int index = mItems.getIndexFromSongId(mCurrentTrack);
			if(index > 0){
				index--;
				mCurrentTrack = mItems.getSongId(index);
			} else {
				mCurrentTrack = mItems.getSongId(
					getRepeatMode() == 0 
					? 0 
					: mItems.size() - 1
				);
			}
			play();
		}
	}
	
	public void next(){
		if(mItems == null){
			prepareList();
			return;
		}
		if(getShuffleMode() == 0){
			int index = mItems.getIndexFromSongId(mCurrentTrack);
			if(index < (mItems.size() - 1)){
				index++;
				mCurrentTrack = mItems.getSongId(index);
			} else {
				mCurrentTrack = mItems.getSongId(0);
				if(getRepeatMode() == 0){
					return;
				}
			}
		} else {
			mCurrentTrack = mItems.getSongId(getRandomSongIndex());
		}
		play();
	}
	
	public void ffwd(){
		setPosition(getPosition() + 10000);
	}
	
	public void rewd(){
		setPosition(getPosition() - 10000);
	}
	
	public void setPosition(int position){
		if(mPlayer != null){
			position = position < 0 
						? 0
						: (position >= getDuration() 
							? getDuration() - 1 
							: position);
			mPlayer.seekTo(position);
			startForeground();
		}
	}
	
	public void setRepeatMode(int mode){
		mLoopMode = mode;
		mPlayer.setLooping(mode == 2);
		startForeground();
		putInt("loopMode",mode);
	}
	
	public void setShuffleMode(int mode){
		mShuffleMode = mode;
		startForeground();
		putInt("shuffleMode",mode);
	}
	
	public int getPosition(){
		if(!isPlaying()){
			return mLastPos;
		}
		return mLastPos = mPlayer.getCurrentPosition();
	}
	
	public int getRepeatMode(){
		return mLoopMode;
	}
	
	public int getShuffleMode(){
		return mShuffleMode;
	}
	
	public int getDuration(){
		return getMusicItem().duration;
	}
	
	private MusicItem getMusicItem(){
		if(mItems == null){
			prepareList();
			return new MusicItem();
		}
		return mItems.getFromSongId(mCurrentTrack);
	}
	
	public String getTitle(){
		return getMusicItem().title;
	}
	
	public String getArtist(){
		return getMusicItem().artist;
	}
	
	public String getAlbum(){
		return getMusicItem().album;
	}
	
	public long getSongId(){
		return getMusicItem().songId;
	}
	
	public long getArtistId(){
		return getMusicItem().artistId;
	}
	
	public long getAlbumId(){
		return getMusicItem().albumId;
	}
	
	public String getPath(){
		return getMusicItem().data;
	}
	
	public String getFilename(){
		return getMusicItem().dispName;
	}
	
	public Bitmap getAlbumArt(){
		return getAlbumArtFromId(getAlbumId());
	}
	
	private Bitmap getDefaultAlbumArt(){
		if(defaultBitmap == null){
			defaultBitmap = BitmapFactory.decodeResource(getResources(),R.drawable.albumart_mp_unknown);
		}
		return defaultBitmap;
	}
	
	public Bitmap getAlbumArtFromId(long albumId){
		if(getBoolean(SettingMap.SET_KEY_LOAD_ALBUM_ART)){
			if(albumId == lastAlbumId){
				return lastBitmap;
			}
			lastAlbumId = albumId;
			try {
				Uri artUri = Uri.parse("content://media/external/audio/albumart");
				artUri = ContentUris.withAppendedId(artUri, albumId);
				return lastBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), artUri);
			} catch(Throwable t){}
		}
		return lastBitmap = getDefaultAlbumArt();
	}
	
	public boolean isPlaying(){
		return mPlayer != null && mPlayer.isPlaying();
	}
	
	private int getRandomSongIndex(){
		int num = (int)(System.currentTimeMillis() % mItems.size());
		return num != mItems.getIndexFromSongId(mOldTrack) ? num : getRandomSongIndex();
	}
	
	private BroadcastReceiver mediaActionReceiver;
	
	private int getInt(String key, int def){
		return MainApp.getDatabase().getInteger(key, def);
	}
	
	private void putInt(String key, int value){
		MainApp.getDatabase().putInteger(key, value);
		MainApp.getDatabase().onlyWrite();
	}
	
	private long getLong(String key, long def){
		return MainApp.getDatabase().getLong(key, def);
	}

	private void putLong(String key, long value){
		MainApp.getDatabase().putLong(key, value);
		MainApp.getDatabase().onlyWrite();
	}
	
	private boolean getBoolean(String key){
		return MainApp.getDatabase().getBoolean(key, SettingMap.getDefault(key));
	}

	private void putBoolean(String key, boolean value){
		MainApp.getDatabase().putBoolean(key, value);
		MainApp.getDatabase().onlyWrite();
	}
	
	private void controlAudioEffects(){
		controlAudioEffects(isPlaying());
	}
	
	private void controlAudioEffects(boolean enable){
		Intent intent = new Intent(enable 
									? AudioEffect.ACTION_OPEN_AUDIO_EFFECT_CONTROL_SESSION 
									: AudioEffect.ACTION_CLOSE_AUDIO_EFFECT_CONTROL_SESSION);
		intent.putExtra(AudioEffect.EXTRA_AUDIO_SESSION, mPlayer.getAudioSessionId());
		intent.putExtra(AudioEffect.EXTRA_PACKAGE_NAME, getPackageName());
		sendBroadcast(intent);
	}
	
	private class MusicServiceBinder extends IMusicService.Stub {
		WeakReference<MusicService> mService;
		
		MusicServiceBinder(MusicService service){
			mService = new WeakReference<MusicService>(service);
		}

		@Override
		public boolean isPlaying() throws RemoteException {
			return mService.get().isPlaying();
		}

		@Override
		public int getSongIndex() throws RemoteException {
			return mService.get().mItems.getIndexFromSongId(mService.get().mCurrentTrack);
		}

		@Override
		public List getSongList() throws RemoteException {
			return mService.get().mItems;
		}

		@Override
		public void play() throws RemoteException {
			mService.get().play();
		}

		@Override
		public void playAt(int index) throws RemoteException {
			mService.get().playAt(index);
		}

		@Override
		public void playFromId(long songId) throws RemoteException {
			mService.get().playFromId(songId);
		}

		@Override
		public void pause() throws RemoteException {
			mService.get().pause();
		}

		@Override
		public void stop() throws RemoteException {
			mService.get().stop();
		}

		@Override
		public void prev() throws RemoteException {
			mService.get().prev();
		}

		@Override
		public void next() throws RemoteException {
			mService.get().next();
		}

		@Override
		public void ffwd() throws RemoteException {
			mService.get().ffwd();
		}

		@Override
		public void rewd() throws RemoteException {
			mService.get().rewd();
		}

		@Override
		public void setPosition(int position) throws RemoteException {
			mService.get().setPosition(position);
		}

		@Override
		public void setRepeatMode(int repeat) throws RemoteException {
			mService.get().setRepeatMode(repeat);
		}

		@Override
		public void setShuffleMode(int shuffle) throws RemoteException {
			mService.get().setShuffleMode(shuffle);
		}

		@Override
		public int getPosition() throws RemoteException {
			return mService.get().getPosition();
		}

		@Override
		public int getRepeatMode() throws RemoteException {
			return mService.get().getRepeatMode();
		}

		@Override
		public int getShuffleMode() throws RemoteException {
			return mService.get().getShuffleMode();
		}

		@Override
		public int getDuration() throws RemoteException {
			return mService.get().getDuration();
		}

		@Override
		public String getTitle() throws RemoteException {
			return mService.get().getTitle();
		}

		@Override
		public String getArtist() throws RemoteException {
			return mService.get().getArtist();
		}

		@Override
		public String getAlbum() throws RemoteException {
			return mService.get().getAlbum();
		}

		@Override
		public long getSongId() throws RemoteException {
			return mService.get().getSongId();
		}

		@Override
		public long getArtistId() throws RemoteException {
			return mService.get().getArtistId();
		}

		@Override
		public long getAlbumId() throws RemoteException {
			return mService.get().getAlbumId();
		}

		@Override
		public String getPath() throws RemoteException {
			return mService.get().getPath();
		}

		@Override
		public String getFileName() throws RemoteException {
			return mService.get().getFilename();
		}

		@Override
		public Bitmap getAlbumArt() throws RemoteException {
			return mService.get().getAlbumArt();
		}

		@Override
		public Bitmap getAlbumArtFromId(long albumId) throws RemoteException {
			return mService.get().getAlbumArtFromId(albumId);
		}
		
		@Override
		public void rebuildNotification() throws RemoteException {
			mService.get().lastAlbumId = -1;
			if(mService.get().notify != null)
				mService.get().startForeground();
		}

	}
	
	public static class MusicList extends ArrayList<MusicItem> {

		public Stream<MusicItem> stream() {
			MusicItem[] arr = new MusicItem[size()];
			return Stream.of(toArray(arr));
		}

		public Stream<MusicItem> parallelStream() {
			return stream();
		}
		
		
		public MusicItem getFromSongId(long songId){
			for(MusicItem item : this){
				if(songId == item.songId){
					return item;
				}
			}
			return get(0);
		}
		
		public int getIndexFromSongId(long songId){
			MusicItem item = getFromSongId(songId);
			if(item != null){
				return indexOf(item);
			}
			return 0;
		}
		
		public long getSongId(int index){
			return get(index).songId;
		}
	}

}
