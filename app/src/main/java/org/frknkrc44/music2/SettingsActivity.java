package org.frknkrc44.music2;

import android.app.*;
import android.os.*;
import org.frknkrc44.music2.adapter.*;

public class SettingsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(SettingsAdapter.getSettingScreen(this));
	}
	
}
