package org.frknkrc44.music2.util;

import android.graphics.*;

public class BitmapUtils {
	
	public static int getBitmapColor(Bitmap bitmap){
		if (bitmap == null) return 0xFFFFFFFF;
		bitmap = Bitmap.createScaledBitmap(bitmap,8,8,true);
		int width = bitmap.getWidth(),height = bitmap.getHeight();
		int[] pixels = new int[width * height];
		bitmap.getPixels(pixels, 0, width, 1, 0, 1, height);
		int color,count = 0,r = 0,g = 0,b = 0,a = 0;
		for(int i = 0;i < pixels.length;i++){
			color = pixels[i];
			a = Color.alpha(color);
			if(a > 0){
				color = (a < 255) ? Color.rgb(Color.red(color),Color.green(color),Color.blue(color)) : color;
				if(color > 0xFF000000){
					r += Color.red(color);
					g += Color.green(color);
					b += Color.blue(color);
					count++;
				}
			}
		}
		if(r == g && g == b && r == 0){
			pixels = new int[width * 2];
			bitmap.getPixels(pixels, 0, width, 0, 0, width, 1);
			for(int i = 0;i < pixels.length;i++){
				color = pixels[i];
				a = Color.alpha(color);
				if(a > 0){
					color = (a < 255) ? Color.rgb(Color.red(color),Color.green(color),Color.blue(color)) : color;
					if(color > 0xFF000000){
						r += Color.red(color);
						g += Color.green(color);
						b += Color.blue(color);
						count++;
					}
				}
			}
		}
		if(r == g && g == b && r == 0){
			count = 1;
		}
		r /= count;
		g /= count;
		b /= count;
		
		color = calculateColors(r,g,b);
		
		for(int i = 1;!ColorUtils.isColorLight(color);i++){
			int m = 0x04 * i;
			color = calculateColors(r+m,g+m,b+m);
		}
		return color;
	}
	
	private static int calculateColors(int r, int g, int b){
		r = r < 256 ? r : 255;
		g = g < 256 ? g : 255;
		b = b < 256 ? b : 255;
		r = (r << 16) & 0x00FF0000;
		g = (g << 8) & 0x0000FF00;
		b = b & 0x000000FF;
		return 0xFF000000 | r | g | b;
	}
}
