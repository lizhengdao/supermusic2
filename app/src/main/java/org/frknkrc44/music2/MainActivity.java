package org.frknkrc44.music2;

import android.*;
import android.app.*;
import android.content.*;
import android.content.pm.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import java.lang.reflect.*;
import java.util.*;
import org.frknkrc44.music2.adapter.*;
import org.frknkrc44.music2.holder.*;
import org.frknkrc44.music2.util.*;

public class MainActivity extends Activity implements ListView.OnItemClickListener, ServiceConnection, View.OnLongClickListener {
	
	@Override
	public void onServiceConnected(ComponentName p1, IBinder p2){
		service = IMusicService.Stub.asInterface(p2);
		listHandler.sendEmptyMessage(0);
	}

	@Override
	public void onServiceDisconnected(ComponentName p1){
		service = null;
		listHandler.removeMessages(0);
		listHandler.removeMessages(1);
	}
	
	@Override
	public boolean onLongClick(View p1){
		switch(p1.getId()){
			case android.R.id.button2:
				try {
					if(service != null){
						unbindService(this);
						stopService(serviceIntent);
					}
					return true;
				} catch(Throwable t){}
				break;
		}
		return false;
	}

	private IMusicService service;
	private boolean seeking, freeze;
	private Intent serviceIntent;
	private IntentFilter iFilter;
	
	private BroadcastReceiver listener = new BroadcastReceiver(){

		@Override
		public void onReceive(Context p1, Intent p2){
			switch(p2.getAction()){
				case MusicService.QUEUE_CHANGED:
					int pos = p2.getIntExtra("currentTrack",0);
					try {
						updateNowPlaying(pos);
					} catch (Throwable e){}
					break;
			}
		}
		
	};
	
	private void updateNowPlaying(int pos) throws Throwable {
		if(service != null){
			View v = findViewById(android.R.id.custom);
			NowPlayingUtils.updateNowPlaying(service, v);
			SeekBar prog = v.findViewById(android.R.id.empty);
			updateProgress();
			if(prog.getTag() == null){
				prog.setTag(0);
				prog.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
					
						private boolean byUser = false;

						@Override
						public void onProgressChanged(SeekBar p1, int p2, boolean p3){
							byUser = p3;
						}

						@Override
						public void onStartTrackingTouch(SeekBar p1){
							seeking = true;
						}

						@Override
						public void onStopTrackingTouch(SeekBar p1){
							if(byUser){
								byUser = false;
								try {
									int p2 = p1.getProgress();
									service.setPosition(p2);
									updateProgress(p2,service.getDuration());
								} catch (RemoteException e){}
							}
							seeking = false;
						}

					});
				ImageButton playBtn = v.findViewById(android.R.id.button2);
				playBtn.setOnLongClickListener(this);
			}
		} else {
			connectToService(false);
		}
	}
	
	private void updateProgress() throws RemoteException {
		if(service != null){
			updateProgress(service.getPosition(), service.getDuration());
		} else {
			connectToService(false);
		}
	}
	
	private void updateProgress(int position, int duration) throws RemoteException {
		if(seeking){
			return;
		}
		View v = findViewById(android.R.id.custom);
		NowPlayingUtils.updateProgress(v, position, duration);
	}
	
	public void controlMedia(View v) throws RemoteException {
		controlMediaFromId(v.getId());
	}
	
	private void controlMediaFromId(int resId) throws RemoteException {
		if(service == null){
			connectToService(false);
			return;
		}
		switch(resId){
			case android.R.id.button1:
				service.prev();
				break;
			case android.R.id.button2:
				if(!listHandler.hasMessages(1)){
					listHandler.sendEmptyMessage(1);
				}
				service.play();
				break;
			case android.R.id.button3:
				service.next();
				break;
			case android.R.id.edit:
				startActivity(new Intent(this,SettingsActivity.class));
				break;
		}
	}

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		ListView list = findViewById(android.R.id.list);
		list.setDrawingCacheEnabled(false);
		list.setAdapter(new SongListAdapter(this));
		list.setOnItemClickListener(this);
		requestPermissionsAndConnect();
    }
	
	private void connectToService(boolean force){
		if(!force) return;
		if(serviceIntent == null){
			serviceIntent = new Intent(this,MusicService.class);
		}
		try {
			startService(serviceIntent);
		} catch(Throwable t){}
		bindService(serviceIntent, this, BIND_EXTERNAL_SERVICE);
		try {
			unregisterReceiver(listener);
		} catch(Throwable t){}
		registerReceiver(listener, iFilter = new IntentFilter(MusicService.QUEUE_CHANGED));
	}

	@Override
	public void onItemClick(AdapterView<?> p1, View p2, int p3, long p4){
		try {
			service.playAt(p3);
		} catch (RemoteException e){}
	}
	
	private Handler listHandler = new Handler(){
		@Override
		public void handleMessage(Message msg){
			if(service == null){
				removeMessages(msg.what);
				return;
			}
			switch(msg.what){
				case 0:
					removeMessages(0);
					try {
						List songs = service.getSongList();
						if(songs == null){
							sendEmptyMessageDelayed(0,100);
						} else {
							ListView list = findViewById(android.R.id.list);
							SongListAdapter adapter = (SongListAdapter) list.getAdapter();
							if(list.getChildCount() > 0){
								adapter.clear();
							}
							adapter.addAll(songs);
							int songIndex = service.getSongIndex();
							updateNowPlaying(songIndex);
							sendEmptyMessage(1);
						}
					} catch(Throwable e){}
					break;
				case 1:
					removeMessages(1);
					if(freeze){
						return;
					}
					try {
						if(service != null){
							if(service.isPlaying()){
								updateProgress();
							}
							sendEmptyMessageDelayed(1,250);
						}
					} catch(RemoteException e){}
					break;
			}
		}
	};
	
	private void requestPermissionsAndConnect(){
		String perm = Manifest.permission.READ_EXTERNAL_STORAGE;
		if(checkCallingOrSelfPermission(perm) != PackageManager.PERMISSION_GRANTED){
			requestPermissions(new String[]{perm},1);
		} else {
			connectToService(true);
		}
	}

	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		requestPermissionsAndConnect();
	}

	@Override
	protected void onPause(){
		super.onPause();
		freeze = true;
		unregisterReceiver(listener);
		unbindService(this);
	}

	@Override
	protected void onResume(){
		super.onResume();
		if(freeze){
			connectToService(true);
			freeze = false;
			if(service != null){
				listHandler.sendEmptyMessage(1);
				if(iFilter != null){
					registerReceiver(listener, iFilter);
					try {
						updateNowPlaying(service.getSongIndex());
					} catch(Throwable e){}
				}
			}
		}
	}

	@Override
	protected void onRestart(){
		super.onRestart();
		if(service != null){
			try {
				service.rebuildNotification();
			} catch(RemoteException e){}
		}
	}
	
}
