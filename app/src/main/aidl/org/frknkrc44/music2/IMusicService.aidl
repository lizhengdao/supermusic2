package org.frknkrc44.music2;

/**
 * Interface for remote access
 * to music service
 *
 * @author frknkrc44
 */

interface IMusicService {
	void play();
	void playAt(int index);
	void playFromId(long songId);
	void pause();
	void stop();
	void prev();
	void next();
	void ffwd();
	void rewd();
	void setPosition(int position);
	void setRepeatMode(int repeat);
	void setShuffleMode(int shuffle);
	int getPosition();
	int getRepeatMode();
	int getShuffleMode();
	int getDuration();
	int getSongIndex();
	String getTitle();
	String getArtist();
	String getAlbum();
	long getSongId();
	long getArtistId();
	long getAlbumId();
	String getPath();
	String getFileName();
	Bitmap getAlbumArt();
	Bitmap getAlbumArtFromId(long albumId);
	List getSongList();
	boolean isPlaying();
	void rebuildNotification();
}
